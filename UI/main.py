from PyQt5 import Qt
from PyQt5 import uic
import sys
import psycopg2


class DB():

    def getData(self, query):
        """ Connect to the PostgreSQL database server """
        conn = None
        try:
            conn = psycopg2.connect(user="postgres",
                                    password="postgres",
                                    host="127.0.0.1",
                                    port="10000",
                                    database="dmd")
            # create a cursor
            cur = conn.cursor()
            cur.execute(query)
            data = cur.fetchall()

            cur.close()
            return data
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        # finally:
        #     if conn is not None:
        #         conn.close()
        #         print('Database connection closed.')


class MyApp(Qt.QMainWindow):

    num_pressed = 0
    db = DB()

    def __init__(self):
        super().__init__()
        uic.loadUi('untitled.ui', self)
        self.initUI()

    def initUI(self):
        self.studentsBtn.clicked.connect(self.studentsAction)
        self.profBtn.clicked.connect(self.professorAction)
        self.crsBtn.clicked.connect(self.crsAction)
        self.listStudBtn.clicked.connect(self.listBestStudents)

    def crsAction(self):
        self.label.setText('List of Courses')
        self.myTbl.clear()
        self.myTbl.setRowCount(0)
        self.myTbl.setColumnCount(4)
        data = self.db.getData('select * from courses')

        self.entriesNum.setText('Number of courses: ' + str(len(data)))

        self.myTbl.setHorizontalHeaderItem(0, Qt.QTableWidgetItem('Course name'))
        self.myTbl.setHorizontalHeaderItem(1, Qt.QTableWidgetItem('Group #s'))
        self.myTbl.setHorizontalHeaderItem(2, Qt.QTableWidgetItem('Core?'))
        self.myTbl.setHorizontalHeaderItem(3, Qt.QTableWidgetItem('Professor ID'))

        self.myTbl.horizontalHeader().setSectionResizeMode(0, Qt.QHeaderView.ResizeToContents)
        self.myTbl.horizontalHeader().setSectionResizeMode(1, Qt.QHeaderView.ResizeToContents)
        self.myTbl.horizontalHeader().setSectionResizeMode(Qt.QHeaderView.Stretch)

        for i in range(len(data)):
            self.myTbl.insertRow(self.myTbl.rowCount())
            self.myTbl.setItem(i, 0, Qt.QTableWidgetItem(str(data[i][1]['name'])))
            self.myTbl.setItem(i, 1, Qt.QTableWidgetItem(str(data[i][1]['groups'])))
            self.myTbl.setItem(i, 2, Qt.QTableWidgetItem(str(data[i][1]['is_core'])))
            self.myTbl.setItem(i, 3, Qt.QTableWidgetItem(str(data[i][1]['professor'])))

    def professorAction(self):
        self.label.setText('List of Professors')
        self.myTbl.clear()
        self.myTbl.setRowCount(0)
        self.myTbl.setColumnCount(3)
        data = self.db.getData('select * from professors')

        self.entriesNum.setText('Number of professors: ' + str(len(data)))

        self.myTbl.setHorizontalHeaderItem(0, Qt.QTableWidgetItem('Full name'))
        self.myTbl.setHorizontalHeaderItem(1, Qt.QTableWidgetItem('Telegram'))
        self.myTbl.setHorizontalHeaderItem(2, Qt.QTableWidgetItem('Phone'))

        self.myTbl.horizontalHeader().setSectionResizeMode(0, Qt.QHeaderView.ResizeToContents)
        self.myTbl.horizontalHeader().setSectionResizeMode(1, Qt.QHeaderView.ResizeToContents)
        self.myTbl.horizontalHeader().setSectionResizeMode(Qt.QHeaderView.Stretch)

        for i in range(len(data)):
            self.myTbl.insertRow(self.myTbl.rowCount())
            self.myTbl.setItem(i, 0, Qt.QTableWidgetItem(str(data[i][1]['full_name'])))
            self.myTbl.setItem(i, 1, Qt.QTableWidgetItem(str(data[i][1]['telegram'])))
            self.myTbl.setItem(i, 2, Qt.QTableWidgetItem(str(data[i][1]['phone'])))


    def studentsAction(self):
        self.label.setText('List of Students')
        self.myTbl.clear()
        self.myTbl.setRowCount(0)
        self.myTbl.setColumnCount(8)
        data = self.db.getData('select * from students')

        self.entriesNum.setText('Number of students: ' + str(len(data)))

        self.myTbl.setHorizontalHeaderItem(0, Qt.QTableWidgetItem('Full name'))
        self.myTbl.setHorizontalHeaderItem(1, Qt.QTableWidgetItem('Telegram'))
        self.myTbl.setHorizontalHeaderItem(2, Qt.QTableWidgetItem('Passport'))
        self.myTbl.setHorizontalHeaderItem(3, Qt.QTableWidgetItem('English'))
        self.myTbl.setHorizontalHeaderItem(4, Qt.QTableWidgetItem('Physics'))
        self.myTbl.setHorizontalHeaderItem(5, Qt.QTableWidgetItem('Math'))
        self.myTbl.setHorizontalHeaderItem(6, Qt.QTableWidgetItem('Prog'))
        self.myTbl.setHorizontalHeaderItem(7, Qt.QTableWidgetItem('Motivation'))

        self.myTbl.horizontalHeader().setSectionResizeMode(0, Qt.QHeaderView.ResizeToContents)
        self.myTbl.horizontalHeader().setSectionResizeMode(1, Qt.QHeaderView.ResizeToContents)
        self.myTbl.horizontalHeader().setSectionResizeMode(Qt.QHeaderView.Stretch)
        self.myTbl.verticalHeader().setSectionResizeMode(Qt.QHeaderView.Stretch)

        for i in range(len(data)):
            self.myTbl.insertRow(self.myTbl.rowCount())
            self.myTbl.setItem(i, 0, Qt.QTableWidgetItem(str(data[i][1]['full_name'])))
            self.myTbl.setItem(i, 1, Qt.QTableWidgetItem(str(data[i][1]['telegram'])))
            self.myTbl.setItem(i, 2, Qt.QTableWidgetItem(str(data[i][1]['passport'])))
            self.myTbl.setItem(i, 3, Qt.QTableWidgetItem(str(data[i][4]['english'])))
            self.myTbl.setItem(i, 4, Qt.QTableWidgetItem(str(data[i][4]['physics'])))
            self.myTbl.setItem(i, 5, Qt.QTableWidgetItem(str(data[i][4]['mathematics'])))
            self.myTbl.setItem(i, 6, Qt.QTableWidgetItem(str(data[i][4]['programming'])))
            self.myTbl.setItem(i, 7, Qt.QTableWidgetItem(str(data[i][4]['motivation'])))


    def listBestStudents(self):
        data = self.db.getData('select * from students')
        scienceScore = int(self.scScore.text())
        englishScore = int(self.engScore.text())
        motivationScore = int(self.motScore.text())

        counter = 0
        for i in range(len(data)):
            if (scienceScore <= (int(data[i][4]['mathematics'])
                                 + int(data[i][4]['programming'])
                                 + int(data[i][4]['physics'])) // 3
                    and englishScore <= int(data[i][4]['english'])
                    and motivationScore <= int(data[i][4]['motivation'])):
                counter += 1


        self.entriesNum.setText('Number of chosen students: ' + str(counter))


def main():
    app = Qt.QApplication([])
    mw = MyApp()
    mw.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()