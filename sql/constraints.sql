CREATE EXTENSION IF NOT EXISTS "postgres-json-schema";

--Students table

--Students Info
ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_info_valid_format;
ALTER TABLE Students ADD CONSTRAINT s_info_valid_format CHECK (
  validate_json_schema('{
    "type":"object",
    "properties":{
      "first_name":{
        "type":"string"
      },
      "last_name":{
        "type":"string"
      },
      "full_name":{
        "type":"string"
      },
      "passport":{
        "type":"string"
      },
      "photo":{
        "type":"string"
      },
      "birthdate":{
        "type":"string"
      },
      "phone":{
        "type":"string"
      },
      "telegram":{
        "type":"string"
      }
    },
    "required":["first_name", "last_name", "passport", "birthdate", "phone"]
  }',info)
);

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_info_first_name;
ALTER TABLE Students ADD CONSTRAINT s_info_first_name CHECK
(LENGTH(info->>'first_name')<=30);

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_info_last_name;
ALTER TABLE Students ADD CONSTRAINT s_info_last_name CHECK
(LENGTH(info->>'last_name')<=30);

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_info_full_name;
ALTER TABLE Students ADD CONSTRAINT s_info_full_name CHECK
(LENGTH(info->>'full_name')<=60);

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_info_passport;
ALTER TABLE Students ADD CONSTRAINT s_info_passport CHECK
(info->>'passport' ~ '[0-9]{4} [0-9]{6}');

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_info_birthday;
ALTER TABLE Students ADD CONSTRAINT s_info_birthday CHECK
(info->>'birthday' ~ '[0-9]{4}-[0-9]{2}-[0-9]{2}');

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_info_phone;
ALTER TABLE Students ADD CONSTRAINT s_info_phone CHECK
(info->>'phone' ~ '[+][0-9]{10}');

--no credentials constraints


--Students apply_info
ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_valid_format;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_valid_format CHECK (
  validate_json_schema('{
    "type":"object",
    "properties":{
      "cv":{
        "type":"string"
      },
      "motivation_letter":{
        "type":"string"
      },
      "groups":{
        "type":"array",
        "items":{
          "type":"number"
        }
      },
      "additional_courses":{
        "type":"array",
        "items":{
          "type":"number"
        }
      },
      "education":{
        "type":"array",
        "items":{
          "type":"object",
          "properties":{
            "name":{
              "type":"string"
            },
            "graduation_year":{
              "type":"number"
            },
            "entrance_year":{
              "type":"number"
            },
            "study_type":{
              "type":"string"
            }
          },
          "required":["name","graduation_year", "entrance_year", "study_type"]
        }
      }
    },
    "required":["cv", "motivation_letter", "groups"]
  }',apply_info)
);

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_cv;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_cv CHECK
(apply_info->>'cv' ~ '/cv/[a-zA-Z\_]{0,40}.docx');

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_ml;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_ml CHECK
(apply_info->>'motivation_letter' ~ '/motivation_letter/[a-zA-Z\_]{0,60}.docx');

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_groups;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_groups CHECK
(jsonb_array_length(all(apply_info->'groups'))<=GROUPS_COUNT());

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_courses;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_courses CHECK
(jsonb_array_length(all(apply_info->'additional_courses'))<=COURSES_COUNT());

-- education
ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_edu_name;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_edu_name CHECK
(LENGTH(apply_info->'education'->>'name')<30);

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_edu_gyear;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_edu_gyear CHECK
(apply_info->'education'->>'graduation_year' ~ '[0-9]{4}');

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_edu_eyear;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_edu_eyear CHECK
(apply_info->'education'->>'entrance_year' ~ '[0-9]{4}');

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_edu_years;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_edu_years CHECK
((apply_info->'education'->>'entrance_year')::integer<=(apply_info->'education'->>'graduation_year')::integer);

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_apply_info_edu_study;
ALTER TABLE Students ADD CONSTRAINT s_apply_info_edu_study CHECK
(LENGTH(apply_info->'education'->>'study_type') <30);

--Students grades
ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_grades_valid_format;
ALTER TABLE Students ADD CONSTRAINT s_grades_valid_format CHECK (
  validate_json_schema('{
    "type":"object",
    "properties":{
      "mathematics":{
        "type":"number"
      },
      "physics":{
        "type":"number"
      },
      "programming":{
        "type":"number"
      },
      "motivation":{
        "type":"number"
      },
      "english":{
        "type":"number"
      }
    },
    "required":["mathematics", "physics", "programming", "motivation", "english"]
  }',grades)
);

ALTER TABLE Students DROP CONSTRAINT IF EXISTS s_grades_correct;
ALTER TABLE Students ADD CONSTRAINT s_grades_correct CHECK
(
  (grades->>'mathematics')::integer>=0 AND
  (grades->>'mathematics')::integer<=100 AND
  (grades->>'physics')::integer>=0 AND
  (grades->>'physics')::integer<=100 AND
  (grades->>'programming')::integer>=0 AND
  (grades->>'programming')::integer<=100 AND
  (grades->>'motivation')::integer>=0 AND
  (grades->>'motivation')::integer<=100 AND
  (grades->>'english')::integer>=0 AND
  (grades->>'english')::integer<=100
);



--Courses table
ALTER TABLE Courses DROP CONSTRAINT IF EXISTS c_info_valid_format;
ALTER TABLE Courses ADD CONSTRAINT c_info_valid_format CHECK (
  validate_json_schema('{
    "type":"object",
    "properties":{
      "professor":{
        "type":"integer"
      },
      "name":{
        "type":"string"
      },
      "is_core":{
        "type":"boolean"
      },
      "groups":{
        "type":"array",
        "items":{
          "type":"number"
        }
      },
      "daytimes":{
        "type":"array",
        "items":{
          "type":"object",
          "properties":{
            "day":{
              "type":"number"
            },
            "time":{
              "type":"string"
            }
          },
          "required":["day", "time"]
        }
      }
      },
      "required":["professor","name", "is_core", "groups", "daytimes"]
    }',info)
);

ALTER TABLE Courses DROP CONSTRAINT IF EXISTS c_info_professor;
ALTER TABLE Courses ADD CONSTRAINT c_info_professor CHECK
((info->>'professor')::integer<=PROFESSORS_COUNT());

ALTER TABLE Courses DROP CONSTRAINT IF EXISTS c_info_name;
ALTER TABLE Courses ADD CONSTRAINT c_info_name CHECK
(LENGTH(info->>'name') <100);

ALTER TABLE Courses DROP CONSTRAINT IF EXISTS c_info_groups;
ALTER TABLE Courses ADD CONSTRAINT c_info_groups CHECK
(jsonb_array_length(all(info->'groups'))<=GROUPS_COUNT());

ALTER TABLE Courses DROP CONSTRAINT IF EXISTS c_info_daytime_day;
ALTER TABLE Courses ADD CONSTRAINT c_info_daytime_day CHECK
((info->'daytimes'->>'day')::integer>=0 AND
(info->'daytimes'->>'day')::integer<=6);

ALTER TABLE Courses DROP CONSTRAINT IF EXISTS c_info_daytime_time;
ALTER TABLE Courses ADD CONSTRAINT c_info_daytime_time CHECK
(info->'daytimes'->>'time' ~ '[0-2][0-4]:[0-5][0-9]');


--Professors Info
ALTER TABLE Professors DROP CONSTRAINT IF EXISTS p_info_valid_format;
ALTER TABLE Professors ADD CONSTRAINT p_info_valid_format CHECK (
  validate_json_schema('{
    "type":"object",
    "properties":{
      "first_name":{
        "type":"string"
      },
      "last_name":{
        "type":"string"
      },
      "full_name":{
        "type":"string"
      },
      "photo":{
        "type":"string"
      },
      "birthdate":{
        "type":"string"
      },
      "phone":{
        "type":"string"
      },
      "telegram":{
        "type":"string"
      }
    },
    "required":["first_name", "last_name", "birthdate", "phone"]
  }',info)
);

ALTER TABLE Professors DROP CONSTRAINT IF EXISTS p_info_first_name;
ALTER TABLE Professors ADD CONSTRAINT p_info_first_name CHECK
(LENGTH(info->>'first_name')<=30);

ALTER TABLE Professors DROP CONSTRAINT IF EXISTS p_info_last_name;
ALTER TABLE Professors ADD CONSTRAINT p_info_last_name CHECK
(LENGTH(info->>'last_name')<=30);

ALTER TABLE Professors DROP CONSTRAINT IF EXISTS p_info_full_name;
ALTER TABLE Professors ADD CONSTRAINT p_info_full_name CHECK
(LENGTH(info->>'full_name')<=60);

ALTER TABLE Professors DROP CONSTRAINT IF EXISTS p_info_birthday;
ALTER TABLE Professors ADD CONSTRAINT p_info_birthday CHECK
(info->>'birthday' ~ '[0-9]{4}-[0-9]{2}-[0-9]{2}');

ALTER TABLE Professors DROP CONSTRAINT IF EXISTS p_info_phone;
ALTER TABLE Professors ADD CONSTRAINT p_info_phone CHECK
(info->>'phone' ~ '[+][0-9]{10}');

--no credentials constraints




ALTER TABLE Groups DROP CONSTRAINT IF EXISTS g_info_valid_format;
ALTER TABLE Groups ADD CONSTRAINT g_info_valid_format CHECK
(validate_json_schema('{
    "type":"object",
    "properties":{
      "degree":{
        "type":"string"
      },
      "track":{
        "type":"string"
      },
      "start_year":{
        "type":"number"
      }
    },
    "required":["degree", "track", "start_year"]
  }',info)
);


ALTER TABLE Groups DROP CONSTRAINT IF EXISTS g_info_degree;
ALTER TABLE Groups ADD CONSTRAINT g_info_degree CHECK
(
  info->>'degree' ~ '[BS|MS|PHD]'
);


ALTER TABLE Logs DROP CONSTRAINT IF EXISTS l_info_valid_format;
ALTER TABLE Logs ADD CONSTRAINT l_info_valid_format CHECK
(validate_json_schema('{
    "type":"object",
    "properties":{
      "datetime":{
        "type":"string"
      },
      "type":{
        "type":"string"
      },
      "student_id":{
        "type":"number"
      }
    },
    "required":["datetime", "type", "student_id"]
  }',info)
);
