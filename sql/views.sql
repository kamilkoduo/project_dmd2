DROP VIEW IF EXISTS Student_Performance;
CREATE VIEW Student_Performance AS
  SELECT info->>'full_name' as Name, grades->>'average' as Score
  FROM Students ORDER BY Score DESC;

DROP VIEW IF EXISTS Group_Courses;
create view Group_Courses as
select groups.info->>'degree' as degree, groups.info->>'track' as track, groups.info->>'start_year' as year,
       c_info->>'name' as name, p_name as professor
from (
    select info as c_info, array_agg(e::text::int) arr
    from courses, jsonb_array_elements(info->'groups') e
    group by 1
    ) courses
  join groups on groups.id = any(arr)
  join (select id as p_id, info->>'full_name' as p_name from professors) prof on p_id = (c_info->>'professor')::int;

DROP VIEW IF EXISTS Professor_Courses;
create view Professor_courses as
  select p_info->>'full_name' as professor, c_info->>'name' as course
  FROM
  (select id as p_id, info as p_info from professors) professors
  JOIN
  (select id as c_id, info as c_info from courses) courses
  on p_id = (c_info->>'professor')::int;