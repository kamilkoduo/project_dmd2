CREATE OR REPLACE FUNCTION fullname_on_create_generate()
  RETURNS TRIGGER AS
$BODY$
BEGIN
  IF NEW.info->'full_name' IS NULL THEN
    NEW.info = NEW.info || jsonb_build_object('full_name', gen_fullname_json(NEW.info));
  end if;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg1 ON Students;

CREATE TRIGGER trg1 BEFORE INSERT
ON Students FOR EACH ROW EXECUTE PROCEDURE fullname_on_create_generate();


CREATE OR REPLACE FUNCTION fullname_on_update_generate()
  RETURNS TRIGGER AS
$BODY$
BEGIN
  IF NEW.info->'full_name' = OLD.info->'full_name' THEN
    IF NEW.info->>'full_name' = gen_fullname_json(OLD.info) THEN
      NEW.info = NEW.info || jsonb_build_object('full_name', gen_fullname_json(NEW.info));
     end if;
  end if;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg2 ON Students;

CREATE TRIGGER trg2 BEFORE UPDATE
  OF info ON Students FOR EACH ROW EXECUTE PROCEDURE fullname_on_update_generate();


create or replace function credentials_on_create_generate()
  returns trigger as
$BODY$
declare
  gen_login TEXT;
begin
  if NEW.credentials->'login' is null then
    loop
      gen_login := concat(NEW.info->>'first_name', '_', random_string(5));
      exit when not (select exists
        (select * from students
        where credentials->>'login' = gen_login))::boolean;
    end loop;
    NEW.credentials = NEW.credentials || jsonb_build_object('login', gen_login);
  end if;
  if NEW.credentials->'password' is null then
    NEW.credentials = NEW.credentials || jsonb_build_object('password', random_string(8));
  end if;
  return NEW;
end;
$BODY$ language plpgsql;

drop trigger if exists trg3 on students;

create trigger trg3 before insert
  on students for each row execute procedure credentials_on_create_generate();


create or replace function student_log()
  returns trigger as
$BODY$
declare
  type TEXT;
begin
  if tg_op = 'INSERT' then
    type := 'student_register';
  elsif tg_op = 'UPDATE' then
    type := 'student_update';
  end if;
  insert into Logs(info) values
    (json_build_object('datetime', to_char(current_timestamp, 'YYYY-MM-DD HH:MM:SS'), 'type', type, 'student_id', NEW.id));
  return NULL;
end;
$BODY$ language plpgsql;

drop trigger if exists trg4 on students;

create trigger trg4 after insert or update
  on students for each row execute procedure student_log();


create or replace function grade_average()
  returns trigger as
$BODY$
declare
  average INTEGER = 0;
begin
  average := (select avg(value::INTEGER) from jsonb_each_text(NEW.grades - 'average'))::INTEGER;
  NEW.grades = NEW.grades || jsonb_build_object('average', average);
  RETURN NEW;
end;
$BODY$ language plpgsql;

drop trigger if exists trg5 on students;

create trigger trg5 before insert or update
  on students for each row execute procedure grade_average();