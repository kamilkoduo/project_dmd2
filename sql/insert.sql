INSERT INTO Professors(credentials, info) VALUES(
  '{}',

  '{"first_name": "Ivan",
  "last_name": "Kifirov",
  "full_name": "Ivan Kifirov",
  "photo": "/photo/ivan_kifirov.png",
  "birthdate": "1987-09-17",
  "phone": "+79193470839",
  "telegram": "@prof1337"}'
);

INSERT INTO Groups(info) VALUES(
   '{"degree": "BS", "track": "SNE", "start_year": 2018}'
);
INSERT INTO Groups(info) VALUES(
   '{"degree": "BS", "track": "DS", "start_year": 2018}'
);
INSERT INTO Groups(info) VALUES(
   '{"degree": "BS", "track": "SE", "start_year": 2018}'
);
INSERT INTO Groups(info) VALUES(
   '{"degree": "BS", "track": "RO", "start_year": 2018}'
);

INSERT INTO Students (info, credentials, apply_info, grades) VALUES (
  '{"first_name": "Nikolai",
  "last_name": "Mikriukov",
  "full_name": "Nikolai Mikriukov",
  "passport": "1234 567890",
  "photo": "/photo/mikr.png",
  "birthdate": "1999-19-08",
  "phone": "+79197170838",
  "telegram": "@Nmikriukov"}',

  '{"login": "",
  "password": ""}',

  '{"cv": "/cv/mikr.docx",
  "motivation_letter": "/motivation_letter/mikr.docx",
  "groups": [1,4],
  "additional_courses": [3, 2],
  "education": [{"name": "17 Gymnasium", "graduation_year": 2012, "entrance_year": 2006, "study_type": "school"},
                {"name": "Innopolis", "graduation_year": 2018, "entrance_year": 2017, "study_type": "institute"}]}',

  '{"mathematics": 89, "english": 54, "physics": 87, "programming": 100, "motivation": 93}'
);

INSERT INTO Courses (info) VALUES
  ('{"professor": 1, "name": "AI", "is_core": true, "groups": [1,2], "daytimes": [{"day": 0, "time": "12:10"}]}'),
  ('{"professor": 2, "name": "Software Project", "is_core": true, "groups": [2], "daytimes": [{"day": 1, "time": "14:10"}, {"day": 2, "time": "18:00"}]}'),
  ('{"professor": 1, "name": "CTF", "is_core": false, "groups": [3,4], "daytimes": [{"day": 3, "time": "09:00"}]}');

INSERT INTO Logs(info) VALUES
  ('{"datetime": "2019-08-23 23:34:56", "type": "student_register", "student_id": 23}'),
  ('{"datetime": "2019-08-24 23:37:53", "type": "student_update", "student_id": 23}');
