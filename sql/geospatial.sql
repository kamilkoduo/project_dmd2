-- functions and types for geospatial search

CREATE TYPE coordinate AS (x real, y real, z real);

CREATE TYPE student_information AS (snd real, fst coordinate, num integer);

-- select student_parameters(id)
CREATE or replace FUNCTION student_parameters (int)
RETURNS coordinate AS $results$
declare
  results coordinate;
begin
  SELECT((grades->>'programming')::real + (grades->>'mathematics')::real + (grades->>'physics')::real) / 3, (grades->>'english')::real, (grades->>'motivation')::real
  INTO results.x, results.y, results.z
  FROM students WHERE id = $1;
  RETURN results;
end;
$results$ LANGUAGE plpgsql

-- select calculate_dist(id, id)
CREATE or replace FUNCTION calculate_dist (integer, integer)
RETURNS real AS $results$
declare
  results real;
begin
  SELECT sqrt(((student_parameters($1)).x - (student_parameters($2)).x)^2 + ((student_parameters($1)).y - (student_parameters($2)).y)^2 + ((student_parameters($1)).z - (student_parameters($2)).z)^2)
  INTO results
  FROM students;
  RETURN results;
end;
$results$ LANGUAGE plpgsql

-- select search_closest_n_students(id, n)
CREATE or replace FUNCTION search_closest_n_students (integer, integer)
RETURNS TABLE (results student_information) AS $func$
declare
  record student_information;
  counter integer := 0;
BEGIN
FOR counter IN 0 .. $2 - 1 LOOP
      RETURN QUERY EXECUTE
      'SELECT calculate_dist(' || $1 || ', students.id), student_parameters(students.id), students.id
       FROM students
       WHERE students.id != ' || $1 || '
       ORDER BY calculate_dist(' || $1 || ', students.id)
       LIMIT 1 OFFSET ' || counter;
  END LOOP;
end
$func$ LANGUAGE plpgsql

-- select search_nth_closest (id, n)
CREATE or replace FUNCTION search_nth_closest (integer, integer)
RETURNS student_information AS $results$
declare
  results student_information;
begin
  SELECT calculate_dist($1, students.id), student_parameters(students.id), students.id
  INTO results
  FROM students
  where students.id != $1
  order by calculate_dist($1, students.id)
  limit 1 offset ($2 - 1);
  RETURN results;
end
$results$ LANGUAGE plpgsql
