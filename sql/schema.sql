CREATE TABLE IF NOT EXISTS Students(
    id SERIAL PRIMARY KEY,
    info JSONB,
    credentials JSONB,
    apply_info JSONB,
    grades JSONB
);

CREATE TABLE IF NOT EXISTS Courses(
    id SERIAL PRIMARY KEY,
    info JSONB
);

CREATE TABLE IF NOT EXISTS Professors(
    id SERIAL PRIMARY KEY,
    info JSONB,
    credentials JSONB
);

CREATE TABLE IF NOT EXISTS Groups(
    id SERIAL PRIMARY KEY,
    info JSONB
);

CREATE TABLE IF NOT EXISTS Logs(
    id SERIAL PRIMARY KEY,
    info JSONB
);
