DROP INDEX IF EXISTS students_login;
DROP INDEX IF EXISTS students_first_name;
DROP INDEX IF EXISTS students_last_name;

CREATE UNIQUE INDEX students_login ON Students ((credentials->'login'));
CREATE INDEX students_first_name ON Students ((info->'first_name'));
CREATE INDEX students_last_name ON Students ((info->'last_name'));
CREATE INDEX students_passport ON students ((info->'passport'));


DROP INDEX IF EXISTS professors_login;
DROP INDEX IF EXISTS professors_first_name;
DROP INDEX IF EXISTS professors_last_name;

CREATE UNIQUE INDEX professors_login ON professors ((credentials->'login'));
CREATE INDEX professors_first_name ON Students ((info->'first_name'));
CREATE INDEX professors_last_name ON Students ((info->'last_name'));


DROP INDEX IF EXISTS students_performance;
CREATE INDEX students_performance ON Students ((grades->'average'));