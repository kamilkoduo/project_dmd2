select * from students;

select avg((grades->>'programming')::integer) from students;

select apply_info->'education'->0->'graduation_year' from students LIMIT 1

select info->>'full_name' as name, ((info->>'birthdate')::date) as birtdate from students ORDER BY birtdate

SELECT Courses.info->>'name' as course, Professors.info->>'full_name' as prof FROM Courses
INNER JOIN Professors ON ((Courses.info->'professor')::integer = Professors.id)
