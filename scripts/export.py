import psycopg2
import zipfile

db_user = "postgres"
db_password = "postgres"
db_host = "127.0.0.1"
db_port = "10000"
db_database = "dmd"
posit = 0


def generate_point(coords):
    global posit
    posit += 1
    return f"""<element type="point3d" label="{str(posit)}">
\t<show object="true" label="false" ev="4"/>
\t<objColor r="{str(int(coords[0] / 100 * 255))}" g="{str(int(coords[1] / 100 * 255))}" b="{str(int(coords[2] / 100 * 255))}" alpha="0"/>
\t<layer val="0"/>
\t<labelMode val="3"/>
\t<animation step="1" speed="1" type="0" playing="false"/>
\t<coords x="{coords[0]}" y="{coords[1]}" z="{coords[2]}" w="1"/>
\t<pointSize val="2"/>
\t<caption val="{str(posit)}"/>
</element>
"""


def export():
    conn = None
    try:
        conn = psycopg2.connect(user=db_user,
                                password=db_password,
                                host=db_host,
                                port=db_port,
                                database=db_database)
        cur = conn.cursor()
        cur.execute("SELECT parameters(id) FROM Students")
        ans = cur.fetchall()
        with open('template.xml') as f:
            data = f.read()
            for student in ans:
                coords = tuple(map(int, student[0][1:-1].split(',')))
                data += generate_point(coords)
        data += "</construction>\n</geogebra>\n"
        with open("geogebra.xml", 'w') as f:
            f.write(data)
        zip_file = zipfile.ZipFile('geogebra.ggb', 'w')
        zip_file.write('geogebra.xml')
        zip_file.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


export()
