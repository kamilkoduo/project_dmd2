#!/usr/bin/python
import psycopg2
from random import randint, choice
import json

db_user = "postgres"
db_password = "postgres"
db_host = "127.0.0.1"
db_port = "10000"
db_database = "postgres"

GENERATE_STUDENTS = 2000
GENERATE_COURSES = 200
GENERATE_PROFESSORS = 100
GENERATE_GROUPS = 50
GENERATE_LOGS = 8000

names = ["James", "John", "Robert", "Michael", "William", "David", "Richard", "Joseph", "Thomas", "Charles",
         "Christopher", "Daniel", "Matthew", "Anthony", "Donald", "Mark", "Paul", "Steven", "Andrew", "Kenneth",
         "George", "Joshua", "Kevin", "Brian", "Edward", "Ronald", "Timothy", "Jason", "Jeffrey", "Ryan", "Jacob",
         "Gary", "Nicholas", "Eric", "Stephen", "Jonathan", "Larry", "Justin", "Scott", "Brandon", "Frank", "Benjamin",
         "Gregory", "Raymond", "Samuel", "Patrick", "Alexander", "Jack", "Dennis", "Jerry", "Tyler", "Aaron", "Henry",
         "Jose", "Douglas", "Peter", "Adam", "Nathan", "Zachary", "Walter", "Kyle", "Harold", "Carl", "Jeremy",
         "Gerald", "Keith", "Roger", "Arthur", "Terry", "Lawrence", "Sean", "Christian", "Ethan", "Austin", "Joe",
         "Albert", "Jesse", "Willie", "Billy", "Bryan", "Bruce", "Noah", "Jordan", "Dylan", "Ralph", "Roy", "Alan",
         "Wayne", "Eugene", "Juan", "Gabriel", "Louis", "Russell", "Randy", "Vincent", "Philip", "Logan", "Bobby",
         "Harry", "Johnny"]
letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
courses = ["Interior Design", "Accounting", "Business Fundamentals", "Finance", "General Business Studies",
           "Advertising", "Entrepreneurship", "Human Resources Management", "International Trade and Commerce",
           "Leadership", "Marketing", "Real Estate", "Human Resources Management", "International Trade and Commerce",
           "Import/Export Operations", "Marketing", "Marketing with Concentration in Digital Marketing",
           "Paralegal Studies", "Personal Financial Planning", "Applications Programming", "Systems Analysis",
           "Design Communication Arts", "Design Communication Arts", "Engineering", "Construction Management",
           "Entertainment Studies", "Acting", "Cinematography", "Directing", "Entertainment Studies", "Film Scoring",
           "Independent Music Production", "Producing", "Music Business", "Screenwriting Film and TV Comprehensive",
           "Journalism", "Post-Baccalaureate Program in Classics", "Landscape Architecture",
           "Pre-Medical and General Science Studies", "Sustainability", "Technical Management", "Project Management",
           "Writers’ Program", "Screenwriting Film and TV Comprehensive", "Fitness Instruction"]

def generate_groups(n):
    conn = None
    try:
        conn = psycopg2.connect(user=db_user,
                                password=db_password,
                                host=db_host,
                                port=db_port,
                                database=db_database)
        cur = conn.cursor()

        for q in range(n):
            info = {"degree": choice(["BS", "MS", "ASP"]),
                    "track": choice(["SNE", "Robotics", "Information Security", "Programming",
                                     "Artificial Intelligence", "Art", "Computer Games", "Computer Vision",
                                     "Sustainability", "Technical Management", "Project Management"]),
                    "start_year": randint(2015, 2020)}

            cur.execute("INSERT INTO Groups (info) VALUES (%s)", (json.dumps(info),))
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def generate_students(n):
    conn = None
    try:
        conn = psycopg2.connect(user=db_user,
                                password=db_password,
                                host=db_host,
                                port=db_port,
                                database=db_database)
        cur = conn.cursor()

        for q in range(n):
            first_name = choice(names)
            second_name = choice(names)
            passport = str(randint(1000, 9999)) + " " + str(randint(100000, 999999))
            birtdate = str(randint(1990, 2006)) + "-" + str(randint(1, 12)).zfill(2) + "-" + str(randint(10, 28))
            phone = "+7" + str(randint(1000000000, 9999999999))
            alias = "@" + ''.join([choice(letters) for _ in range(randint(5, 10))])

            login = first_name + ''.join([choice(letters) for _ in range(5)])
            password = ''.join([choice(letters) for _ in range(10)])

            info = {"first_name": first_name,
                    "last_name": second_name,
                    "full_name": first_name + " " + second_name,
                    "passport": passport,
                    "photo": "/photo/" + first_name + "_" + second_name + ".png",
                    "birthdate": birtdate,
                    "phone": phone,
                    "telegram": alias}

            credentials = {"login": login, "password": password}

            apply_info = {"cv": "/cv/" + first_name + "_" + second_name + ".docx",
                          "motivation_letter": "/motivation_letter/" + first_name + "_" + second_name + ".docx",
                          "groups": [randint(1, 50) for _ in range(randint(1, 5))],
                          "additional_courses": [randint(1, 200) for _ in range(randint(1, 5))],
                          "education": [{"name": f"{randint(1, 100)} {choice(['school', 'gymnasium', 'internat'])}",
                                         "graduation_year": randint(2008, 2016), "entrance_year": randint(2000, 2007),
                                         "study_type": choice(['school', 'gymnasium', 'internat'])}]}

            grades = {"mathematics": randint(70, 100) if randint(1, 10) > 3 else randint(0, 70),
                      "english": randint(60, 100) if randint(1, 10) > 8 else randint(0, 60),
                      "physics": randint(70, 100) if randint(1, 10) > 3 else randint(0, 70),
                      "programming": randint(70, 100) if randint(1, 10) > 3 else randint(0, 70),
                      "motivation": randint(0, 100)}

            cur.execute("INSERT INTO Students (info, credentials, apply_info, grades) VALUES (%s, %s, %s, %s)",
                        (json.dumps(info), json.dumps(credentials), json.dumps(apply_info), json.dumps(grades)))
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def generate_courses(n):
    conn = None
    try:
        conn = psycopg2.connect(user=db_user,
                                password=db_password,
                                host=db_host,
                                port=db_port,
                                database=db_database)
        cur = conn.cursor()

        for q in range(n):
            info = {"professor": randint(1, 100), "name": choice(courses), "is_core": choice([True, False]),
                    "groups": [randint(1, 50) for _ in range(randint(1, 5))],
                    "daytimes": [{"day": randint(0, 6), "time": str(randint(10, 23)) + ":" + str(randint(10, 59))},
                                  {"day": randint(0, 6), "time": str(randint(10, 23)) + ":" + str(randint(10, 59))}]}
            cur.execute("INSERT INTO Courses (info) VALUES (%s)", (json.dumps(info),))
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def generate_professors(n):
    conn = None
    try:
        conn = psycopg2.connect(user=db_user,
                                password=db_password,
                                host=db_host,
                                port=db_port,
                                database=db_database)
        cur = conn.cursor()

        for q in range(n):
            first_name = choice(names)
            second_name = choice(names)
            phone = "+7" + str(randint(1000000000, 9999999999))
            alias = "@" + ''.join([choice(letters) for _ in range(randint(5, 10))])
            birtdate = str(randint(1990, 2006)) + "-" + str(randint(10, 12)) + "-" + str(randint(10, 28))

            login = first_name + ''.join([choice(letters) for _ in range(5)])
            password = ''.join([choice(letters) for _ in range(10)])

            info = {"first_name": first_name,
                    "last_name": second_name,
                    "full_name": first_name + " " + second_name,
                    "photo": "/photo/" + first_name + "_" + second_name + ".png",
                    "birthdate": birtdate,
                    "phone": phone,
                    "telegram": alias}

            credentials = {"login": login, "password": password}

            cur.execute("INSERT INTO Professors (credentials, info) VALUES (%s, %s)", (json.dumps(credentials), json.dumps(info)))
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()




def generate_logs(n):
    conn = None
    try:
        conn = psycopg2.connect(user=db_user,
                                password=db_password,
                                host=db_host,
                                port=db_port,
                                database=db_database)
        cur = conn.cursor()

        for q in range(n):
            date = str(randint(2017, 2019)) + "-" + str(randint(1, 12)).zfill(2) + "-" + str(randint(10, 28))
            time = str(randint(10, 23)) + ":" + str(randint(10, 59)) + ":" + str(randint(10, 59))
            info = {"datetime": date + " " + time,
                    "type": choice(["student_insert", "student_update"]),
                    "student_id": randint(1, GENERATE_STUDENTS)}

            cur.execute("INSERT INTO Logs(info) VALUES (%s)", (json.dumps(info),))
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


generate_professors(GENERATE_PROFESSORS)
generate_courses(GENERATE_COURSES)
generate_groups(GENERATE_GROUPS)
generate_students(GENERATE_STUDENTS)
generate_logs(GENERATE_LOGS)
