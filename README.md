To setup DB with PG Admin you need:

Run: `docker-compose up -d --build`


To execute queries open pgadmin `http://127.0.0.1:10001` with credentials `asd@asd.asd` / `123456` and add server

address: name of postgres container (mb projectdmd2develop_db_1 or similar. Run `docker ps`)

login: `postgres`
pass: `postgres`

Or you can use PSQL:
Run docker exec -it <container_name> bash  
Run psql -U postgres postgres  

Run in PSQL to build the database:  
`\i schema.sql`  
`\i functions.sql`  
`\i index.sql`  
`\i constraints.sql`  
`\i triggers.sql`  
`\i rules.sql`  
`\i views.sql`  
`\i geospatial.sql`  

Code to connect from python to db:

```python
#!/usr/bin/python
import psycopg2


def get_version():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(user="postgres",
                                password="postgres",
                                host="127.0.0.1",
                                port="10000",
                                database="name of your db")
        # create a cursor
        cur = conn.cursor()
        # execute a statement
        print('Connected successfully')
        cur.execute('SELECT version()')
        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)
        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
```
