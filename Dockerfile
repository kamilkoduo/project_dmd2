FROM postgres:11-alpine

RUN apk add git

WORKDIR /ext

RUN git clone https://github.com/gavinwahl/postgres-json-schema.git

RUN apk add make

WORKDIR /ext/postgres-json-schema

RUN make install

WORKDIR /app
